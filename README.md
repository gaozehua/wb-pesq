# README #
ITU-T Recommendation P.862.2: Wideband extension to Recommendation P.862 for the assessment of wideband telephone networks and speech codecs
WB-PESQ +wb +16000 [input pcm] [decode pcm]
ITU-T Recommendation P.862: Perceptual evaluation of speech quality (PESQ): An objective method for end-to-end speech quality assessment of narrow-band telephone networks and speech codecs
WB-PESQ +16000 [input 16khz pcm] [decode pcm]
WB-PESQ +8000 [input 8khz pcm] [decode pcm]